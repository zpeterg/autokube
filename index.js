const fs = require("fs").promises;
const { exec } = require("child_process");
const axios = require("axios");
const templates = require("./templates");

const fileName = "./repo_list.json";

const getList = () => {
  return new Promise(resolve => {
    fs.readFile(fileName)
      .then(list => {
        resolve(JSON.parse(list));
      })
      .catch(() => {
        resolve([]);
      });
  });
};

const saveLocalList = content => {
  console.log("Saving local list...", content);
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, JSON.stringify(content))
      .then(list => {
        resolve(list);
      })
      .catch(e => {
        reject("There was an error writing the new list");
      });
  });
};

const deleteLocalFiles = name => {
  console.log(`Deleting files for ${name}`);
  const promises = [fs.unlink(`ser-${name}.yml`), fs.unlink(`dep-${name}.yml`)];
  return Promise.all(promises);
};

const getRepoList = () => {
  return new Promise((resolve, reject) => {
    axios
      .get("https://gitlab.com/api/v4/users/zpeterg/projects")
      .then(content => {
        console.log("Repo list:", content.data);
        resolve(content.data);
      })
      .catch(e => {
        reject("There was an error in calling the repo list");
      });
  });
};

const diff = (localList, remoteList) => {
  let rtn = {
    update: [],
    remove: []
  };
  remoteList.map(r => {
    const matchingLocal = localList.find(l => l.name === r.name);
    console.log(
      `Last activity: local ${matchingLocal &&
        matchingLocal.last_activity_at}, ${r.last_activity_at}`
    );
    // Purposefully add everything to update here - to get around throttling on last_activity_at
    if (
      1 === 1 ||
      !matchingLocal ||
      r.last_activity_at > matchingLocal.last_activity_at
    ) {
      rtn.update.push(r);
    }
  });
  rtn.remove = localList.filter(l => !remoteList.find(r => r.name === l.name));
  return rtn;
};

const kubDelete = ({ name, type }) => {
  console.log(`Deleting from kubernetes with ${name}, ${type}`);
  return new Promise((resolve, reject) => {
    const cmd = `kubectl delete ${
      type === "deployment" ? "deployments" : "services"
    } ${type === "deployment" ? "dep" : "ser"}-${name}`;
    exec(cmd, (err, _, stderr) => {
      if (err) reject(`Unable to run command ${cmd}, ${err}`);
      if (stderr) reject(`Error with running command ${cmd}, ${stderr}`);
      resolve();
    });
  });
};

const kubUpdate = ({ name, type }) => {
  console.log(`Updating on kubernetes with ${name}, ${type}`);
  return new Promise((resolve, reject) => {
    const cmd = `kubectl apply -f ${
      type === "deployment" ? "dep" : "ser"
    }-${name}.yml`;
    exec(cmd, (err, _, stderr) => {
      if (err) reject(`Unable to run command ${cmd}, ${err}`);
      if (stderr) reject(`Error with running command ${cmd}, ${stderr}`);
      resolve();
    });
  });
};

const saveFile = ({
  containerPort,
  nodePort,
  content,
  name,
  replicas,
  type,
  version
}) => {
  let template = "";
  if (type === "service")
    template = templates.templateSer({ containerPort, name, nodePort });
  else
    template = templates.templateDep({
      containerPort,
      name,
      replicas,
      version
    });
  console.log("Saving file...", name);
  const thisFileName = `${type === "deployment" ? "dep" : "ser"}-${name}.yml`;
  return new Promise((resolve, reject) => {
    fs.writeFile(thisFileName, template)
      .then(() => {
        resolve();
      })
      .catch(e => {
        reject(`There was an error writing file ${thisFileName}`);
      });
  });
};

const getRepoFile = (url, name) => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${url}/raw/master/kub_settings.json`)
      .then(({ data }) => {
        console.log(
          `Saving service/deployment files for ${name}:${data.version}`
        );
        if (data && data.containerPort && data.version) {
          Promise.all([
            saveFile({
              containerPort: data.containerPort,
              nodePort: data.nodePort,
              name,
              type: "service"
            }),
            saveFile({
              containerPort: data.containerPort,
              name,
              type: "deployment",
              replicas: data.replicas,
              version: data.version
            })
          ])
            .then(() => {
              resolve();
            })
            .catch(e => {
              reject(
                "There was an error saving the service or deployment files"
              );
            });
        }
      })
      .catch(e => {
        reject(
          `There was an error obtaining the repository kub_settings.json for ${name}:${version}: ${e}`
        );
      });
  });
};

const runUpdate = (web_url, name) => {
  return getRepoFile(web_url, name)
    .then(() => {
      return kubUpdate({ name, type: "service" });
    })
    .then(() => {
      kubUpdate({ name, type: "deployment" });
    });
};

const runRemove = name => {
  if (!name) return;
  console.log(`Running remove on service ${name}`);
  return kubDelete({ name, type: "service" }).then(() => {
    console.log(`Running remove on deployment ${name}`);
    return kubDelete({ name, type: "deployment" }).then(() =>
      deleteLocalFiles(name)
    );
  });
};

// Run it all
const run = () => {
  return new Promise(resolve => {
    Promise.all([getList(), getRepoList()])
      .then(res => {
        const localList = res[0];
        const remoteList = res[1]
          // only repos starting with 's'
          .filter(({ name }) => {
            return name.substring(0, 5) === "fish-";
          })
          // extract only name, url, and last-activity
          .map(({ name, web_url, last_activity_at }) => {
            return {
              name,
              web_url,
              last_activity_at
            };
          });
        console.log("Remote List...", remoteList);
        console.log("Local List...", localList);
        // If they're not equal, step through looking for the differences
        const diffed = diff(localList, remoteList);
        // Run all of these Promises at the same time
        const tasks = [];
        console.log("Updating...", diffed.update);
        diffed.update.map(u => {
          tasks.push(runUpdate(u.web_url, u.name));
        });
        console.log("Removing...", diffed.remove);
        diffed.remove.map(r => {
          tasks.push(runRemove(r.name));
        });
        tasks.push(saveLocalList(remoteList));
        return Promise.all(tasks);
      })
      .then(() => {
        console.log("---------- DONE ----------");
      })
      .catch(e => {
        console.log(e);
      })
      .finally(() => {
        resolve();
      });
  });
};

// Loop the man run() function above
const watch = () => {
  run().then(() => {
    setTimeout(() => {
      watch();
    }, 10 * 1000);
  });
};
watch();
