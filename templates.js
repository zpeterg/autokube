exports.templateDep = ({ containerPort, name, replicas, version }) => `
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dep-${name}
  labels:
    app: ${name}
spec:
  replicas: ${replicas}
  selector:
    matchLabels:
      app: ${name}
  template:
    metadata:
      labels:
        app: ${name}
    spec:
      containers:
        - name: ${name}
          image: registry.gitlab.com/zpeterg/${name}:${version}
          ports:
            - containerPort: ${containerPort}

`;

exports.templateSer = ({ containerPort, name, nodePort }) => `
apiVersion: v1
kind: Service
metadata:
  name: ser-${name}
spec:
  selector:
    app: ${name}
  ports:
    - name: http
      protocol: TCP
      port: ${containerPort}
      targetPort: ${containerPort}
      nodePort: ${nodePort}
  type: LoadBalancer

`;
